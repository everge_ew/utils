package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.emerywaterhouse.utils.DbUtils;

/**
 * @author Eric Verge
 *
 */
public class RestrictionLoader {
   
   public static enum Environment {
      Test,
      Production
   };
   
   protected Environment m_Env;
   private Connection m_Conn;
   private PreparedStatement 
         m_InsertItemRestr,
         m_InsertCustRestrMD;

   public RestrictionLoader() {

   }
   
   private void close() {
      DbUtils.closeDbConn(m_Conn, m_InsertItemRestr, null);
   }
   
   /**
    * Gets a connection to the proper database depending on the current Environment.
    * 
    * @return true if a connection was established, false otherwise
    */
   private boolean getConnection() {
      boolean connected = false;
      
      try {
         DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

         if (m_Env == Environment.Production) {
            m_Conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.9:1521:GROK", "eis_emery", "boxer");
         } else if (m_Env == Environment.Test) {
            m_Conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.127:1521:DANA", "everge", "verge");
         }
         
         if (m_Conn != null) {
            m_Conn.setAutoCommit(false);
            connected = true;
         }
         
      } catch (Exception ex) {
         //m_Log.error("[initLoader]", ex);
         connected = false;
      }
      
      return connected;
   }
   
   /**
    * Builds PreparedStatement objects from the SQL queries needed to add the data
    * to the database.
    * 
    * @return true if all statements were successfully prepared
    */
   private boolean prepareStatements() {
      boolean prepared = false;
      
      try {
         m_InsertItemRestr = m_Conn.prepareStatement(
               "insert into item_restriction (item_id, grant_id) " +
               "select ?, ? " +
               "from dual " +
               "where not exists ( " +
               "   select null from item_restriction where item_id = ? and grant_id = ?) "
         );
         
         m_InsertCustRestrMD = m_Conn.prepareStatement(
               "insert into cust_grant (customer_id, grant_id) " +
               "select ?, 28 " +
               "from dual " +
               "where not exists ( " +
               "   select null from cust_grant where customer_id = ? and grant_id = 28) "
         );
         
         prepared = true;
      }

      catch ( Exception ex ) {
         //m_Log.error("[prepareStatements]", ex);
      }

      return prepared;
   }
   
   /**
    * Adds data to the database
    * @throws SQLException 
    */
   private int[] loadItems() throws SQLException {
      int[] insertCount = {0, 0};
      
      Statement stmt1 = m_Conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY),
                stmt2 = m_Conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY),
                stmt3 = m_Conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
      
      ResultSet
         itemIDSet = stmt1.executeQuery("select item_id from everge.restr_items"),
         grantIDSet = stmt2.executeQuery("select grant_id from everge.grants"),
         custIDSet = stmt3.executeQuery("select customer_id from everge.cust_md");
      
      if (prepareStatements()) {
         try {
            while (itemIDSet.next()) {
               m_InsertItemRestr.setString(1, itemIDSet.getString(1));
               m_InsertItemRestr.setString(3, itemIDSet.getString(1));
               
               while (grantIDSet.next()) {
                  m_InsertItemRestr.setInt(2, grantIDSet.getInt(1));
                  m_InsertItemRestr.setInt(4, grantIDSet.getInt(1));
                  
                  insertCount[0] += m_InsertItemRestr.executeUpdate();
               }
               grantIDSet.beforeFirst();
            }
            
            while (custIDSet.next()) {
               m_InsertCustRestrMD.setString(1, custIDSet.getString(1));
               m_InsertCustRestrMD.setString(2, custIDSet.getString(1));
               
               insertCount[1] += m_InsertCustRestrMD.executeUpdate();
            }
            
            //m_Conn.commit();
            
         } catch (SQLException e) {
            // TODO Auto-generated catch block
            try {
               m_Conn.rollback();
            } catch (SQLException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
            }
            e.printStackTrace();
         } finally {
            itemIDSet = null;
            grantIDSet = null;
            custIDSet = null;
         }
      }
      return insertCount;
   }

   /**
    * Parses the arguments passed to main via the command line.
    * Sets the appropriate member variables
    * 
    * @param args  arguments passed on the command line
    * @return  true if arguments were successfully parsed, otherwise false
    */
   private boolean parseCommandLine(String[] args) {
      int pcount = 0;
      boolean argsOk = true;

      if ( args != null )
         pcount = args.length;

      //
      // The first argument is the environment, second is the file name.
      if ( pcount == 1 ) {
         if (args[0].equalsIgnoreCase("test")) {
            m_Env = Environment.Test;
         } else if ( args[0].equalsIgnoreCase("production") ) {
            m_Env = Environment.Production;
         } else {
            argsOk = false;
         }
      
      } else {
         System.out.println("usage: loaditems [env]");
         System.out.println("example: loaditems test");
         System.out.println(String.format("arg count %d", args.length));
         argsOk = false;
      }

      return argsOk;
   }

   
   /**
    * Starts the process of adding data to the database
    * 
    * @param args  arguments passed via command line. Environment first
    */
   public static void main(String[] args) {
      RestrictionLoader loader = new RestrictionLoader();
      int[] insertCount = {0, 0};
      Scanner sc = new Scanner(System.in);
      
      try {
         if (loader.parseCommandLine(args) && loader.getConnection()) {
            insertCount = loader.loadItems();
         }
         
         System.out.println("Rows inserted: \n   Item Restrictions: " + insertCount[0] + 
               "\n   Customer Restrictions: " + insertCount[1]);
         System.out.print("\n\nCommit? ");
         char doIt = sc.next().toLowerCase().charAt(0);
         
         if (doIt == 'y') {
            loader.m_Conn.commit();
            System.out.println("Committed");
         } else {
            loader.m_Conn.rollback();
            System.out.println("Rolled back");
         }
         
      } catch (Exception ex) {
         ex.printStackTrace(System.err);
      } finally {
         loader.close();
         loader = null;
         sc.close();
      }
      
      
   }
}

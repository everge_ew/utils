package utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.emerywaterhouse.utils.DbUtils;

/**
 * @author Eric Verge
 *
 */
public class ShowNameLoader {
   
   public static enum Environment {
      Test,
      Production
   };
   
   protected Environment m_Env;
   private String m_FilePath;
   private Connection conn;
   private PreparedStatement
         showNameUpdate,
         showNameInsert;

   public ShowNameLoader() {

   }
   
   private void close() {
      DbUtils.closeDbConn(null, showNameUpdate, null);
      try {
         conn.close();
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
   
   /**
    * Gets a connection to the proper database depending on the current Environment.
    * 
    * @return true if a connection was established, false otherwise
    */
   private boolean getConnection() {
      boolean connected = false;
      
      try {
         DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

         if (m_Env == Environment.Production) {
            conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.9:1521:GROK", "eis_emery", "boxer");
         } else if (m_Env == Environment.Test) {
            conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.127:1521:DANA", "eis_emery", "mugwump");
         }
         
         if (conn != null) {
            conn.setAutoCommit(false);
            connected = true;
         }
         
      } catch (Exception ex) {
         //m_Log.error("[initLoader]", ex);
         connected = false;
      }
      
      return connected;
   }
   
   /**
    * Builds PreparedStatement objects from the SQL queries needed to add the data
    * to the database.
    * 
    * @return true if all statements were successfully prepared
    */
   private boolean prepareStatements() {
      boolean prepared = false;
      
      try {
         showNameUpdate = conn.prepareStatement(
               "update show_cust " +
               "set show_name = ? " +
               "where customer_id = ? ");
         
         showNameInsert = conn.prepareStatement(
               "insert into show_cust " +
               "(customer_id, show_id, show_name) " +
               "values " +
               "(?, 20, ?) ");
         
         prepared = true;
      }

      catch ( Exception ex ) {
         //m_Log.error("[prepareStatements]", ex);
      }

      return prepared;
   }
   
   /**
    * Parses data from the Workbook and adds it to the database
    */
   private void loadItems() {
      Workbook book = null;
      Sheet sheet = null;
      Row row = null;
      CellReference // these store references to the columns we wish to parse
            showNameCol = new CellReference("B1"),
            custIDCol = new CellReference("C1");
            //parentIDCol = new CellReference("F1");
      String
            showName = null,
            custID = null;
      int
         //parentID = 0,
         rowCount = 0;
      
      if (prepareStatements()) {
         try {
            book = new XSSFWorkbook(new File(m_FilePath));
            sheet = book.getSheetAt(0);
            rowCount = sheet.getPhysicalNumberOfRows();
            
            for (int rowNum = 1; rowNum < rowCount; rowNum++) {
               row = sheet.getRow(rowNum);
               
               showName = row.getCell(showNameCol.getCol()).getStringCellValue();
               custID = row.getCell(custIDCol.getCol()).getStringCellValue();
               
               if (!custID.equalsIgnoreCase("PROSPECT")) {
                  showNameUpdate.setString(1, showName);
                  showNameUpdate.setString(2, custID);
                  if (showNameUpdate.executeUpdate() == 0) {
                     //parentID = Integer.parseInt(row.getCell(parentIDCol.getCol()).getStringCellValue());
                     showNameInsert.setString(1, custID);
                     //showNameInsert.setInt(2, parentID);
                     showNameInsert.setString(2, showName);
                     showNameInsert.executeUpdate();
                  };
               }
            }
            
            conn.commit();
            
         } catch (InvalidFormatException | IOException | SQLException e) {
            // TODO Auto-generated catch block
            try {
               conn.rollback();
            } catch (SQLException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
            }
            e.printStackTrace();
         } finally {
            close();
            row = null;
            sheet = null;
            book = null;
         }
      }
   }
      
   /**
    * Parses the arguments passed to main via the command line.
    * Sets the appropriate member variables
    * 
    * @param args  arguments passed on the command line
    * @return  true if arguments were successfully parsed, otherwise false
    */
   private boolean parseCommandLine(String[] args) {
      int pcount = 0;
      boolean argsOk = true;

      if ( args != null )
         pcount = args.length;

      //
      // The first argument is the environment, second is the file path.
      if ( pcount == 2 ) {
         if (args[0].equalsIgnoreCase("test")) {
            m_Env = Environment.Test;
         } else if ( args[0].equalsIgnoreCase("production") ) {
            m_Env = Environment.Production;
         } else {
            argsOk = false;
         }

         m_FilePath = args[1];
      
      } else {
         System.out.println("usage: loaditems [env] [updatefile]");
         System.out.println("example: loaditems test /documents/aceitems.xlsx");
         System.out.println(String.format("arg count %d", args.length));
         argsOk = false;
      }

      return argsOk;
   }

   
   /**
    * Starts the process of adding data from an Excel file to the database
    * 
    * @param args  arguments passed via command line. Environment first, path to Excel file second
    */
   public static void main(String[] args) {
      ShowNameLoader loader = new ShowNameLoader();
      
      try {
         if (loader.parseCommandLine(args) && loader.getConnection()) {
            loader.loadItems();
         }
      } catch (Exception ex) {
         ex.printStackTrace(System.err);
      } finally {
         loader = null;
      }
   }
}

package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class AddressSpreadsheetUpdater {
   
   private static final int CUSTOMER_ID_FIELD = 7;
   private static final String 
      DATABASE_URL = "jdbc:oracle:thin:@10.128.0.9:1521:GROK",
      USER_NAME = "everge",
      USER_PASSWORD = "verge",
      SELECT_ACTIVE_IDS_QUERY = "select customer_id " +
                                "from customer " +
                                "join cust_address using(customer_id) " +
                                "join cust_market using(customer_id) " +
                                "where cust_status_id = 1 and description = 'STORE ADDRESS' " +
                                "  and market_id = 4 and mkt_class_id not in (141, 101, 26, 27, 34, 24) ",
      SELECT_ADDRESSES_QUERY = "select name, description, addr1, city, state, postal_code, customer_id, addr2 " +
                               "from customer " +
                               "join cust_address using(customer_id) " +
                               "where cust_status_id = 1 and customer_id = ? and description = 'STORE ADDRESS' "; 
   
   private static PreparedStatement selectAddresses;
   private Workbook 
      oldWorkbook,
      newWorkbook;
   //private Sheet sheet;
   private Connection connection;
   
   public static void update(String filePath) throws Exception{
      new AddressSpreadsheetUpdater(filePath).update();
   }
   
   public AddressSpreadsheetUpdater(String filePath) throws Exception {
      oldWorkbook = new XSSFWorkbook(filePath);
      newWorkbook = new XSSFWorkbook();
      connection = getConnection();
      selectAddresses = connection.prepareStatement(SELECT_ADDRESSES_QUERY, ResultSet.TYPE_SCROLL_INSENSITIVE);
   }
   
   public void update() throws Exception {
      buildUpdatedWorkbook(getNewIDs());
      
   }
   
   public void buildUpdatedWorkbook(Collection<String> newCustomerIDs) throws Exception {
      Sheet sheet = newWorkbook.createSheet();
      
      addOriginalAddresses(sheet);
      addNewAddresses(sheet, newCustomerIDs);
      
      FileOutputStream outFile = new FileOutputStream(new File("C:\\Users\\everge\\update.xls"));
      newWorkbook.write(outFile);
      outFile.close();
   }
   
   public void addOriginalAddresses(Sheet newSheet) {
      int rowCount = 0;
      Row newRow = null;
      
      for (Row oldRow : oldWorkbook.getSheetAt(0)) {
         int cellCount = 0;
         
         newRow = newSheet.createRow(rowCount++);
         for (Cell cell : oldRow) {
            newRow.createCell(cellCount++).setCellValue(getCellValueAsString(cell));
         }
      }
   }
   
   public void addNewAddresses(Sheet newSheet, Collection<String> newIDs) throws Exception {
      ResultSet newCustomerData;
      
      for (String newID : newIDs) {
         newCustomerData = loadCustomerData(newID);
         //newCustomerData.first();
         newCustomerData.next();
         Row newRow = newSheet.createRow(newSheet.getLastRowNum() + 1);
         for (int i = 0; i <= 7; i++) {
            String temp = (newCustomerData.getString(i+1) == null ? newCustomerData.getString(i+1) : newCustomerData.getString(i+1).toUpperCase());
            newRow.createCell(i).setCellValue(temp);
         }
         
      }
   }
   
   private ResultSet loadCustomerData(String customerID) throws Exception {
      selectAddresses.setString(1, customerID);
      return selectAddresses.executeQuery();
   }
   
   public Collection<String> getNewIDs() throws Exception {
      Collection<String> newIDs = loadActiveIDs();
      Collection<String> oldIDs = loadOriginalIDs();
//      for (String ID : newIDs) {
//         if (oldIDs.contains(ID)) {
//            newIDs.remove(ID);
//         }
//      }
      
      newIDs.removeAll(loadOriginalIDs());
      return newIDs;
   }

   public Collection<String> loadActiveIDs() throws Exception {
      //PreparedStatement selectActiveIDs = connection.prepareStatement(SELECT_ACTIVE_IDS_QUERY);
      ResultSet activeIDs = connection.prepareStatement(SELECT_ACTIVE_IDS_QUERY).executeQuery();//selectActiveIDs.executeQuery();
      Collection<String> loadedIDs = new ArrayList<String>();
      while (activeIDs.next()) {
         loadedIDs.add(activeIDs.getString("customer_id").trim());
      }
      return loadedIDs;
   }

   public Collection<String> loadOriginalIDs() {
      Sheet sheet = oldWorkbook.getSheetAt(0);
      Collection<String> originalIDs = new ArrayList<String>();
      
      for (Row row : sheet) {
         if (row.getRowNum() != 0)
            originalIDs.add(loadIDFrom(row).trim());
      }
      return originalIDs;
   }
   
   public String loadIDFrom(Row row){
      //int test = row.getCell(CUSTOMER_ID_FIELD).getCellType();
      //int rownum = row.getRowNum();
      return getCellValueAsString(row.getCell(CUSTOMER_ID_FIELD));
   }
   
   public String getCellValueAsString(Cell cell) {
      switch (cell.getCellType()) {
         case Cell.CELL_TYPE_STRING:
            return cell.getStringCellValue().toUpperCase();
         case Cell.CELL_TYPE_NUMERIC:
            return Double.toString(cell.getNumericCellValue()).toUpperCase();
         default:
            return cell.getStringCellValue().toUpperCase();
      }
   }

   private Connection getConnection() throws SQLException {
      DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
      Connection connection = DriverManager.getConnection(DATABASE_URL, USER_NAME, USER_PASSWORD);
      connection.setAutoCommit(false);
      return connection;
   }

   public static void main(String[] args) {
      try {
         update(args[0]);
      } catch (Exception e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

}

package utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.emerywaterhouse.utils.DbUtils;

/**
 * @author Eric Verge
 *
 */
public class TaxThreeLoader {
   
   public static enum Environment {
      Test,
      Production
   };
   
   protected Environment m_Env;
   private String m_FilePath;
   private Connection m_Conn;
   private PreparedStatement
         m_TaxThreeUpd,
         m_TaxThreeIns,
         m_BMIUpd,
         m_BMIIns,
         m_FLCUpd,
         m_ItemWarehouseUpd;

   public TaxThreeLoader() {

   }
   
   private void close() {
      DbUtils.closeDbConn(null, m_TaxThreeUpd, null);
      DbUtils.closeDbConn(null, m_TaxThreeIns, null);
      DbUtils.closeDbConn(null, m_BMIUpd, null);
      DbUtils.closeDbConn(null, m_BMIIns, null);
      DbUtils.closeDbConn(null, m_FLCUpd, null);
      DbUtils.closeDbConn(null, m_ItemWarehouseUpd, null);
      try {
         m_Conn.close();
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
   
   /**
    * Gets a connection to the proper database depending on the current Environment.
    * 
    * @return true if a connection was established, false otherwise
    */
   private boolean getConnection() {
      boolean connected = false;
      
      try {
         DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

         if (m_Env == Environment.Production) {
            m_Conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.9:1521:GROK", "eis_emery", "boxer");
         } else if (m_Env == Environment.Test) {
            m_Conn = DriverManager.getConnection ("jdbc:oracle:thin:@10.128.0.127:1521:DANA", "eis_emery", "mugwump");
         }
         
         if (m_Conn != null) {
            m_Conn.setAutoCommit(false);
            connected = true;
         }
         
      } catch (Exception ex) {
         //m_Log.error("[initLoader]", ex);
         connected = false;
      }
      
      return connected;
   }
   
   /**
    * Builds PreparedStatement objects from the SQL queries needed to add the data
    * to the database.
    * 
    * @return true if all statements were successfully prepared
    */
   private boolean prepareStatements() {
      boolean prepared = false;
      
      try {
         m_TaxThreeUpd = m_Conn.prepareStatement(
               "update item_taxthree set taxthree_id = ? where item_id = ? ");

         m_TaxThreeIns = m_Conn.prepareStatement(
               "insert into item_taxthree values (?, ?) ");
         
         m_BMIUpd = m_Conn.prepareStatement(
               "update bmi_item set noun = ?, modifier = ? where item_id = ? ");
         
         m_BMIIns = m_Conn.prepareStatement(
               "insert into bmi_item (noun, modifier) values(?, ?) where item_id = ? ");
         
         m_FLCUpd = m_Conn.prepareStatement(
               "update item set flc_id = ? where item_id = ? ");
         
         m_ItemWarehouseUpd = m_Conn.prepareStatement(
               "update item_warehouse set in_catalog = 1 where item_id = ? ");
         
         prepared = true;
      }

      catch ( Exception ex ) {
         //m_Log.error("[prepareStatements]", ex);
      }

      return prepared;
   }
   
   /**
    * Parses data from the Workbook and adds it to the database
    */
   private void loadItems() {
      Workbook book = null;
      Sheet sheet = null;
      Row row = null;
      CellReference // these store references to the columns we wish to parse
            itemIDCol = new CellReference("D1"),
            taxThreeIDCol = new CellReference("J1"),
            flcIDCol = new CellReference("L1"),
            nounCol = new CellReference("N1"),
            modifierCol = new CellReference("O1");
      String
            itemID = null,
            flcID = null,
            noun = null,
            modifier = null;
      int 
            taxThreeID = 0,
            rowCount = 0;
      
      if (prepareStatements()) {
         try {
            book = new XSSFWorkbook(new File(m_FilePath));
            sheet = book.getSheetAt(0);
            rowCount = sheet.getPhysicalNumberOfRows();
            Cell cell = null;
            
            for (int rowNum = 1; rowNum < rowCount; rowNum++) {
               row = sheet.getRow(rowNum);
               
               itemID = row.getCell(itemIDCol.getCol()).getStringCellValue();
               
               // load Tax Three data
               cell = row.getCell(taxThreeIDCol.getCol());
               taxThreeID = (cell == null || cell.getStringCellValue().equals("")) ? 0 : Integer.parseInt(cell.getStringCellValue());
               addTaxThree(itemID, taxThreeID);
               
               // load FLC data
               cell = row.getCell(flcIDCol.getCol());
               flcID = (cell == null ? "" : cell.getStringCellValue());
               addFLC(itemID, flcID);
               
               // load BMI data
               cell = row.getCell(nounCol.getCol());
               noun = (cell == null ? "" : cell.getStringCellValue());
               cell = row.getCell(modifierCol.getCol());
               modifier = (cell == null ? "" : cell.getStringCellValue());
               addBMI(itemID, noun, modifier);
               
               // load Item Warehouse data
               m_ItemWarehouseUpd.setString(1, itemID);
               m_ItemWarehouseUpd.executeUpdate();
            }
            
            m_Conn.commit();
            
         } catch (InvalidFormatException | IOException | SQLException e) {
            // TODO Auto-generated catch block
            try {
               m_Conn.rollback();
            } catch (SQLException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
            }
            e.printStackTrace();
         } finally {
            close();
            row = null;
            sheet = null;
            book = null;
         }
      }
   }
   
   /**
    * Adds the given data to the ITEM_TAXTHREE table of the database
    * 
    * @param itemID  the Emery item number to be added or updated 
    * @param taxThreeID  the TaxThreeID for the item corresponding to itemID
    */
   private void addTaxThree(String itemID, int taxThreeID) {
      try {
         if (taxThreeID != 0) {
            m_TaxThreeUpd.setInt(1, taxThreeID);
            m_TaxThreeUpd.setString(2, itemID);
            
            // if the update didn't modify any records, perform an insert
            if (m_TaxThreeUpd.executeUpdate() == 0) {
               m_TaxThreeIns.setString(1, itemID);
               m_TaxThreeIns.setInt(2, taxThreeID);
               m_TaxThreeIns.executeUpdate();
            }
         }
      } catch (SQLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
   
   /**
    * Adds the given data to the BMI_ITEM table of the database
    * 
    * @param itemID  the Emery item number to be added or updated 
    * @param noun  BMI noun to describe the item
    * @param modifier  BMI modifier to add detail to item description
    */
   private void addBMI(String itemID, String noun, String modifier) {
      try {
         if (noun != null && !noun.equals(""))
            m_BMIUpd.setString(1, noun);
         else
            m_BMIUpd.setNull(1, Types.VARCHAR);
         
         if (modifier != null && !modifier.equals(""))
            m_BMIUpd.setString(2, modifier);
         else
            m_BMIUpd.setNull(2, Types.VARCHAR);
         
         m_BMIUpd.setString(3, itemID);
         
         // if the update didn't modify any records, perform an insert
         if (m_BMIUpd.executeUpdate() == 0) {
            if (noun != null && !noun.equals(""))
               m_BMIIns.setString(1, noun);
            else
               m_BMIIns.setNull(1, Types.VARCHAR);
            
            if (modifier != null && !modifier.equals(""))
               m_BMIIns.setString(2, modifier);
            else
               m_BMIIns.setNull(2, Types.VARCHAR);
            
            m_BMIIns.setString(3, itemID);
            m_BMIIns.executeUpdate();
         }
      } catch (SQLException e) {
         e.printStackTrace();
      }
   }
   
   private void addFLC(String itemID, String flc) {
      try {
         if (flc != null && !flc.equals("")) 
            m_FLCUpd.setString(1, flc);
         else
            m_FLCUpd.setNull(1, Types.VARCHAR);
         
         m_FLCUpd.setString(2, itemID);
         
         m_FLCUpd.executeUpdate();
      } catch (SQLException e){
         e.printStackTrace();
      }
   }
   
   /**
    * Parses the arguments passed to main via the command line.
    * Sets the appropriate member variables
    * 
    * @param args  arguments passed on the command line
    * @return  true if arguments were successfully parsed, otherwise false
    */
   private boolean parseCommandLine(String[] args) {
      int pcount = 0;
      boolean argsOk = true;

      if ( args != null )
         pcount = args.length;

      //
      // The first argument is the environment, second is the file name.
      if ( pcount == 2 ) {
         if (args[0].equalsIgnoreCase("test")) {
            m_Env = Environment.Test;
         } else if ( args[0].equalsIgnoreCase("production") ) {
            m_Env = Environment.Production;
         } else {
            argsOk = false;
         }

         m_FilePath = args[1];
      
      } else {
         System.out.println("usage: loaditems [env] [updatefile]");
         System.out.println("example: loaditems test /documents/aceitems.xlsx");
         System.out.println(String.format("arg count %d", args.length));
         argsOk = false;
      }

      return argsOk;
   }

   
   /**
    * Starts the process of adding data from an Excel file to the database
    * 
    * @param args  arguments passed via command line. Environment first, path to Excel file second
    */
   public static void main(String[] args) {
      TaxThreeLoader loader = new TaxThreeLoader();
      
      try {
         if (loader.parseCommandLine(args) && loader.getConnection()) {
            loader.loadItems();
         }
      } catch (Exception ex) {
         ex.printStackTrace(System.err);
      } finally {
         loader = null;
      }
   }
}

package utils;

/**
 * Contains some utilities to make working with proper names slightly simpler.
 * @author Eric Verge
 */
public class NameUtils {
   
   /**
    * A convenience method to title case the input using the default delimiter, " " (space).
    * Equivalent to calling titleCase(input, null).
    * @param input  the String to be titled-cased. A null String is treated like the 
    *               empty String.
    * @return the input with all characters following spaces in proper title case and
    *         all other characters in lower case.
    */
   public static String titleCase(String input) {
      return titleCase(input, null);
   }
   
   /**
    * Puts into title case any character that immediately follows a delimiter.
    * @param input  the String to be titled-cased. A null String is treated like the 
    *               empty String.
    * @param delimiters  Strings that constitute delimiters which should be followed
    *                    by a title case character. Consists only of the space character
    *                    (" ") when passed a null.
    * @return the input with all characters following delimiters in proper title case and
    *         all other characters in lower case.
    */
   public static String titleCase(String input, String[] delimiters) {
      input = (input == null ? "" : input.trim());
      
      if (input.isEmpty()) {
         return input;
      }
      if (delimiters == null) {
         delimiters = new String[]{" "};
      }
      return titleCaseIter(input.toLowerCase(), delimiters);
   }
   
   /**
    * Recursive method that is responsible for all of the actual work involved in putting a
    * String into proper title case.
    * @param input  the String to be titled-cased. Can't be null.
    * @param delimiters  Strings that should be followed by title case letters
    * @return the input with the initial character and all characters following delimiters 
    *         in proper title case
    */
   private static String titleCaseIter(String input, String[] delimiters) {
      input = capitalizeFirst(input);
      int targetCharacter = findNextTargetCharacter(input, delimiters);
      
      if (targetCharacter >= 0) {
         // re-call this method on the substring immediately following earliest delimiter
         return input.substring(0, targetCharacter) 
               + titleCaseIter(input.substring(targetCharacter), delimiters);
      }
      // no delimiters found, return input String with first letter in title case
      return input;
   }
   
   /**
    * Puts the first character of input into proper title case
    * @param input  the String to be capitalized
    * @return the original String with the first character in title case
    */
   public static String capitalizeFirst(String input) {
      if (input.length() < 1) {
         return input;
      }
      char[] inputChars = input.toCharArray();
      inputChars[0] = Character.toTitleCase(inputChars[0]);
      
      return new String(inputChars);
   }
   
   /**
    * Helper method to find the earliest character in the input that immediately
    * follows a delimiter.
    * @param input  the String to be searched for the next target character
    * @param delimiters  Strings which should always be followed by capital letters
    * @return the position of the next Character that needs to be title cased
    */
   private static int findNextTargetCharacter(String input, String[] delimiters) {
      int 
         match = -1,
         earliestMatch = input.length(),
         target = -1;
      for (String delimiter : delimiters) {
         match = input.indexOf(delimiter);
         
         if (-1 < match && match < earliestMatch) {
            earliestMatch = match;
            target = match + delimiter.length();
         }
      }
      return target;
   }
   
   /**
    * Tests if the input String consists solely of capital letters.
    * @param input  the String being tested
    * @return true if all characters in the input are upper case letters as
    *         defined in the Unicode specification
    */
   public static boolean isAllCapital(String input) {
      for (char letter : getLetters(input)) {
         if (!Character.isUpperCase(letter)) {
            return false;
         }
      }
      return true;
   }
   
   /**
    * Tests if the input String consists solely of lower case letters.
    * @param input  the String being tested
    * @return if all characters in the input are lower case letters as
    *         defined in the Unicode specification
    */
   public static boolean isAllLower(String input) {
      for (char letter : getLetters(input)) {
         if (!Character.isLowerCase(letter)) {
            return false;
         }
      }
      return true;
   }
   
   /**
    * Tests if the input String contains characters in a mix of cases.
    * @param input  the String being tested
    * @return true if input consists of characters in at least two different
    *         cases as defined in the Unicode specification
    */
   public static boolean isMixedCase(String input) {
      if (input.length() <= 1) {
         return false;
      }
      
      char[] letters = getLetters(input);
      int initialType = Character.getType(letters[0]);
      
      for (char letter : letters) {
         if (Character.getType(letter) != initialType) {
            return true;
         }
      }
      
      return false;
   }
   
   public static char[] getLetters(String input) {
      return input.replaceAll("[\\W\\d]", "").toCharArray();
   }

   
   public static void main(String[] args) {
      String 
         test = "this is my test string",
         name = "  t.C. mAcMiLlAn MCdOuGaL-o'cOnNeR ǆEvAdA dEwItT ";
      String[] delimiters = new String[]{" ", ".", "'", "-", "De", "Mc", "Mac"};

      System.out.println(capitalizeFirst(test));
      System.out.println(titleCase(test));
      
      System.out.println(titleCase(name));
      System.out.println(titleCase(name, delimiters));
      
      for (String tester : new String[] {"test", "TEST", "Test", "Yet another test, man", "ONE. MORE."}) {
         System.out.println("\nTest string: " + tester);
         System.out.println("isAllCapital: " + isAllCapital(tester));
         System.out.println("isAllLower: " + isAllLower(tester));
         System.out.println("isMixedCase: " + isMixedCase(tester));
      }
   }

}
